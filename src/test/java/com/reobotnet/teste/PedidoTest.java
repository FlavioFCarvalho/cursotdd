package com.reobotnet.teste;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.reobotnet.desconto.CalculaFaixaDescontoTerceiraFaixa;
import com.reobotnet.desconto.CalculadoraFaixaDesconto;
import com.reobotnet.desconto.CalculadoraFaixaDescontoPrimeiraFaixa;
import com.reobotnet.desconto.CalculadoraFaixaDescontoSegundaFaixa;
import com.reobotnet.desconto.SemDesconto;
import com.reobotnet.model.Pedido;
import com.reobotnet.model.ItemPedido;
import com.reobotnet.model.ResumoPedido;

public class PedidoTest {

	private Pedido pedido;

	@Before
	public void setup() {
		CalculadoraFaixaDesconto calculadoraFaixaDesconto = 
				new CalculaFaixaDescontoTerceiraFaixa(
					new CalculadoraFaixaDescontoSegundaFaixa(
						new CalculadoraFaixaDescontoPrimeiraFaixa(
							new SemDesconto(null))));
			
			pedido = new Pedido(calculadoraFaixaDesconto);

	}

	private void assertResumoDoPedido(double valorTotal, double desconto) {
		ResumoPedido resumoPedido = pedido.resumo();

		assertEquals(valorTotal, resumoPedido.getValorTotal(), 0.001);
		assertEquals(desconto, resumoPedido.getDesconto(), 0.0001);
	}

	@Test
	public void devePermitirAdicionarUmItemNoPedido() throws Exception {

		pedido.adicionarItem(new ItemPedido("Sabonte", 3.0, 10));

	}

	@Test
	public void deveCalcularValorTotalEDescontoParaPedidoVazio()
			throws Exception {

		assertResumoDoPedido(0.0, 0.0);

	}

	@Test
	public void testName() throws Exception {

	}

	@Test
	public void deveCalcularParaUmOuMaisItemSemDesconto() throws Exception {
		pedido.adicionarItem(new ItemPedido("Perfume", 3.0, 3));
		// pedido.adicionarItem(new itemPedido("Óleo para o corpo", 7.0, 3));

		assertResumoDoPedido(9.0, 0);
	}

	@Test
	public void deveApicarDescontoNa1Faixa() throws Exception {

		pedido.adicionarItem(new ItemPedido("Tablet", 40.0, 10));

		assertResumoDoPedido(400.0, 16.0);

	}

	@Test
	public void deveApicarDescontoNa2Faixa() throws Exception {

		pedido.adicionarItem(new ItemPedido("Óleo", 15.0, 30));
		pedido.adicionarItem(new ItemPedido("Shampoo", 15.0, 30));
		assertResumoDoPedido(900.0, 54.0);

	}
	
	@Test
	public void deveApicarDescontoNa3Faixa() throws Exception {

		pedido.adicionarItem(new ItemPedido("Óleo", 15.0, 30));
		pedido.adicionarItem(new ItemPedido("Shampoo", 15.0, 30));
		pedido.adicionarItem(new ItemPedido("Escova", 10.0, 30));
		assertResumoDoPedido(1200.0, 96.0);

	}
	
	

}
