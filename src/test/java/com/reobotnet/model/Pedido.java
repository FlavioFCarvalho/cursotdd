package com.reobotnet.model;

import java.util.ArrayList;
import java.util.List;

import com.reobotnet.desconto.CalculadoraFaixaDesconto;

public class Pedido {

	private double valorTotal;

	private List<ItemPedido> itens = new ArrayList<>();
	
	private CalculadoraFaixaDesconto calculadoraFaixaDesconto;
	

	public Pedido(CalculadoraFaixaDesconto calculadoraFaixaDesconto) {
		this.calculadoraFaixaDesconto = calculadoraFaixaDesconto;
	}

	public void adicionarItem(ItemPedido itemPedido) {

		itens.add(itemPedido);
	}

	public ResumoPedido resumo() {

		valorTotal = itens.stream()
				.mapToDouble(i -> i.getValorUnitario() * i.getQuantidade())
				.sum();
		double desconto = calculadoraFaixaDesconto.desconto(valorTotal);

//		if (valorTotal > 300 && valorTotal <= 800) {
//			desconto = valorTotal * 0.04;
//		} else if (valorTotal > 800 && valorTotal <= 1000) {
//			desconto = valorTotal * 0.06;
//		}else if (valorTotal > 1000){
//			desconto = valorTotal * 0.08;
//		}
		return new ResumoPedido(valorTotal, desconto);
	}

}
