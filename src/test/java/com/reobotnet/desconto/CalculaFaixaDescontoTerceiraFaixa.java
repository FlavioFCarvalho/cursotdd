package com.reobotnet.desconto;

public class CalculaFaixaDescontoTerceiraFaixa extends CalculadoraFaixaDesconto {

	public CalculaFaixaDescontoTerceiraFaixa(CalculadoraFaixaDesconto proximo) {
		super(proximo);
		
	}

	@Override
	protected double calcular(double valorTotal) {
		
		if (valorTotal > 1000)
			return valorTotal * 0.08;
		
		return -1;
		
	}

}
